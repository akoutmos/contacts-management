from peewee import *

database = SqliteDatabase('phone_books.db')

class BaseModel(Model):
    database = database

    class Meta:
        database = database

