from peewee import *
from base_model import BaseModel
from contacts import Contacts

class PhoneNumbers(BaseModel):
    contact = ForeignKeyField(Contacts, related_name='phone_numbers')
    phone_number = CharField()
