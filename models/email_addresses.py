from peewee import *
from base_model import BaseModel
from contacts import Contacts

class EmailAddresses(BaseModel):
    contact = ForeignKeyField(Contacts, related_name='emails')
    email = CharField()
