from peewee import *
from base_model import BaseModel
from phone_books import PhoneBooks

class Contacts(BaseModel):
    phone_book = ForeignKeyField(PhoneBooks, related_name='contacts')
    uuid = UUIDField(index=True, unique=True)
    first_name = CharField()
    last_name = CharField()
    birthday = CharField()
