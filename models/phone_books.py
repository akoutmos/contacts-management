from peewee import *
from base_model import BaseModel

class PhoneBooks(BaseModel):
    uuid = UUIDField(index=True, unique=True)
    creation_date = DateTimeField()
