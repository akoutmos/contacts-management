from peewee import *
from base_model import BaseModel
from contacts import Contacts

class Addresses(BaseModel):
    contact = ForeignKeyField(Contacts, related_name='addresses')
    street = CharField()
    suite = CharField()
    state = CharField()
    city = CharField()
    zip_code = CharField()
