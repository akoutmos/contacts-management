from flask import Flask, jsonify, abort, render_template, request
from peewee import *
from playhouse.shortcuts import model_to_dict, dict_to_model
from jsonschema import validate
from models import *

import uuid
import datetime

database = BaseModel.database

app = Flask(__name__,
            static_folder = './assets/static',
            template_folder = './assets/templates')

@app.before_request
def before_request():
    database.connect()

@app.after_request
def after_request(response):
    database.close()
    return response

# -------- Create phone book --------
@app.route('/api/v1/phone-books', methods=['POST'])
def create_phone_book():
    resp = ''
    status = 400

    new_phonebook = PhoneBooks(uuid = uuid.uuid4(),
                               creation_date = datetime.datetime.now())

    try:
        new_phonebook.save()
        resp = jsonify({'phone_book_id': new_phonebook.uuid})
        status = 201
    except:
        status = 409

    return resp, status

# ------- Get contacts from phone book --------
@app.route('/api/v1/phone-books/', methods=['GET'])
@app.route('/api/v1/phone-books/<phone_book_id>', methods=['GET'])
def view_phone_book(phone_book_id=''):
    resp = ''
    status = 400

    try:
        phone_book = (PhoneBooks
                     .select()
                     .join(Contacts, JOIN.LEFT_OUTER)
                     .join(Addresses, JOIN.LEFT_OUTER).switch(Contacts)
                     .join(EmailAddresses, JOIN.LEFT_OUTER).switch(Contacts)
                     .join(PhoneNumbers, JOIN.LEFT_OUTER).switch(Contacts)
                     .where(PhoneBooks.uuid == phone_book_id)
                     .get())

        phone_book_data = model_to_dict(phone_book,
                                        exclude = [PhoneBooks.id, PhoneBooks.creation_date,
                                                   Contacts.id, PhoneNumbers.id, EmailAddresses.id,
                                                   Addresses.id],
                                        backrefs = True)
        resp = jsonify(phone_book_data['contacts'])
        status = 200

    except:
        status = 404

    return resp, status

# -------- Create phone book entry --------
@app.route('/api/v1/phone-books/<phone_book_id>/contacts', methods=['POST'])
def create_phone_book_contact(phone_book_id):
    resp = ''
    status = 400

    schema = {
        'type': 'object',
        'properties': {
            'first_name': {'type': 'string'},
            'last`_name': {'type': 'string'},
            'birthday': {'type': 'string'},

            'addresses': {
                'properties': {
                    'street': {'type': 'string', 'minLength': 1},
                    'suite': {'type': 'string', 'minLength': 1},
                    'city': {'type': 'string', 'minLength': 1},
                    'state': {'type': 'string', 'minLength': 1},
                    'zip_code': {'type': 'string', 'pattern': '^\d{5}$'},
                },
                'required': ['street', 'city', 'state', 'zip_code']
            },

            'emails': {
                'type': 'array',
                'minItems': 1,
                'items': {
                    'type': 'string',
                    'pattern': '^.+@.+\..+$'
                }
            },

            'phone_numbers': {
                'type': 'array',
                'minItems': 1,
                'items': {
                    'type': 'string',
                    'pattern': '^\d{10}$'
                }
            }
        },
        'required': ['first_name', 'last_name', 'birthday', 'emails', 'phone_numbers']
    }

    try:
        json_request = request.get_json()
        validate(json_request, schema)

        with database.atomic() as transaction:
            try:
                phone_book = (PhoneBooks
                     .select()
                     .where(PhoneBooks.uuid == phone_book_id)
                     .get())

                # Create contact
                contact = Contacts(phone_book_id = phone_book.id,
                                   uuid = uuid.uuid4(),
                                   first_name = json_request['first_name'],
                                   last_name = json_request['last_name'],
                                   birthday = json_request['birthday'])
                contact.save()

                # Create phone numbers
                for phone_number in json_request['phone_numbers']:
                    entry = PhoneNumbers(contact_id = contact.id,
                                         phone_number = phone_number)
                    entry.save()

                # Create emails
                for email in json_request['emails']:
                    entry = EmailAddresses(contact_id = contact.id,
                                            email = email)
                    entry.save()

                # Create addresses
                for address in json_request['addresses']:
                    entry = Addresses(contact_id = contact.id,
                                      street = address['street'],
                                      suite = address['suite'],
                                      city = address['city'],
                                      state = address['state'],
                                      zip_code = address['zip_code'])

                    entry.save()


                resp = jsonify(model_to_dict(contact,
                                             exclude = [PhoneBooks.id, PhoneBooks.creation_date,
                                                        Contacts.id, PhoneNumbers.id, EmailAddresses.id,
                                                        Addresses.id],
                                             backrefs = True))
                status = 201
            except Exception as e:
                database.rollback()
                status = 503

    except Exception as e:
        status = 401

    return resp, status

# -------- Delete phone book contact --------
@app.route('/api/v1/phone-books/<phone_book_id>/contacts/<contact_id>', methods=['DELETE'])
def delete_phone_book_contact(phone_book_id, contact_id):
    status = 400

    try:
        contact = (Contacts
                   .select()
                   .join(Addresses, JOIN.LEFT_OUTER).switch(Contacts)
                   .join(EmailAddresses, JOIN.LEFT_OUTER).switch(Contacts)
                   .join(PhoneNumbers, JOIN.LEFT_OUTER).switch(Contacts)
                   .where(Contacts.uuid == contact_id)
                   .get())

        contact = model_to_dict(contact, recurse = True, backrefs = True)

        if str(contact['phone_book']['uuid']) != phone_book_id:
            status = 404
        else:
            with database.atomic() as transaction:
                try:
                    # Remove all phone #s
                    for phone in contact['phone_numbers']:
                        (PhoneNumbers
                        .delete()
                        .where(PhoneNumbers.id == phone['id'])
                        .execute())

                    # Remove all addresses
                    for address in contact['addresses']:
                        (Addresses
                        .delete()
                        .where(Addresses.id == address['id'])
                        .execute())

                    # Remove all emails
                    for email in contact['emails']:
                        (EmailAddresses
                        .delete()
                        .where(EmailAddresses.id == email['id'])
                        .execute())

                    (Contacts
                    .delete()
                    .where(Contacts.uuid == contact_id)
                    .execute())

                    status = 204

                except Exception as e:
                    database.rollback()
                    status = 503
    except Exception as e:
        print e
        status = 404

    return '', status

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")
