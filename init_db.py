from models import *

database = BaseModel.database

def init_db():
    database.create_tables([Addresses, Contacts, EmailAddresses, PhoneBooks, PhoneNumbers], safe=True)

if __name__ == '__main__':
    init_db()
