import Vue from 'vue'
import Router from 'vue-router'
import CreatePhoneBook from '@/components/CreatePhoneBook'
import ViewPhoneBook from '@/components/ViewPhoneBook'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'CreatePhoneBook',
      component: CreatePhoneBook
    },
    {
      path: '/:phone_book_id',
      name: 'ViewPhoneBook',
      component: ViewPhoneBook
    }
  ]
})
