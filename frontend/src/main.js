import Vue from 'vue'
import App from './App'
import Router from './router'
import { Loading } from 'element-ui'
import Store from './store'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import 'normalize.css/normalize.css'
import 'font-awesome/css/font-awesome.css'
import 'typeface-roboto'
import 'typeface-indie-flower'

Vue.config.productionTip = false

Vue.use(Loading)

// Configure language
locale.use(lang)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: Router,
  render: h => h(App),
  store: Store
})
