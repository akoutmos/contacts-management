import {
  CREATE_PHONE_BOOK,
  CREATE_CONTACT,
  SET_CONTACTS,
  SET_PHONE_BOOK_ID,
  CLEAR_CONTACTS,
  ADD_CONTACT,
  DELETE_CONTACT,
  REMOVE_CONTACT,
  ARE_CONTACTS_LOADED,
  GET_SORTED_CONTACTS,
  GET_PHONE_BOOK,
  GET_PHONE_BOOK_ID
} from './PhoneBook.types'
import axios from 'axios'

export default {
  namespaced: true,
  state: {
    phone_book_id: '',
    contacts: null
  },
  getters: {
    [GET_PHONE_BOOK_ID]: state => state.phone_book_id,
    [ARE_CONTACTS_LOADED]: state => state.contacts !== null,
    [GET_SORTED_CONTACTS]: (state, getters) => {
      let result = null

      if (getters[ARE_CONTACTS_LOADED]) {
        // Group contacts together by last name
        let aggregated_contacts = state.contacts
          .reduce((acc, contact) => {
            let last_name = contact.last_name
            let contact_key = last_name.match(/^[A-Za-z]/) ? last_name.charAt(0).toUpperCase() : '#'

            if (acc.hasOwnProperty(contact_key)) {
              acc[contact_key].push(contact)
            } else {
              acc[contact_key] = [contact]
            }

            return acc
          }, {})

        // Sort the "keys" of the contacts (A-Z and # for other),
        // and then sort the contact partitions themselves
        result = Object.entries(aggregated_contacts)
          .sort(([key1, value1], [key2, value2]) => {
            return key1.localeCompare(key2)
          })
          .map(([key, value]) => {
            let sorted_contacts_partition = value
              .sort(({ last_name: lname1 }, { last_name: lname2 }) => lname1.localeCompare(lname2))

            return [key, sorted_contacts_partition]
          })
      }

      return result
    }
  },
  mutations: {
    [SET_CONTACTS] (state, contacts) {
      contacts.forEach((contact) => {
        contact.emails = contact.emails.map(entry => entry.email)
        contact.phone_numbers = contact.phone_numbers.map(entry => entry.phone_number)
      })
      state.contacts = contacts
    },
    [SET_PHONE_BOOK_ID] (state, phone_book_id) {
      state.phone_book_id = phone_book_id
    },
    [CLEAR_CONTACTS] (state) {
      state.contacts = null
      state.phone_book_id = ''
    },
    [ADD_CONTACT] (state, contact) {
      contact.emails = contact.emails.map(entry => entry.email)
      contact.phone_numbers = contact.phone_numbers.map(entry => entry.phone_number)
      state.contacts.push(contact)
    },
    [REMOVE_CONTACT] (state, contact_id) {
      state.contacts = state.contacts.filter(contact => contact_id !== contact.uuid)
    }
  },
  actions: {
    [GET_PHONE_BOOK]: ({ commit }, phone_book_id) => {
      return new Promise((resolve, reject) => {
        axios
          .get(`/api/v1/phone-books/${phone_book_id}`)
          .then(({ data }) => {
            commit(SET_CONTACTS, data)
            commit(SET_PHONE_BOOK_ID, phone_book_id)
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    [CREATE_PHONE_BOOK]: ({ commit }) => {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/v1/phone-books`)
          .then(({ data }) => {
            commit(SET_CONTACTS, [])
            commit(SET_PHONE_BOOK_ID, data.phone_book_id)
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    [CREATE_CONTACT]: ({ commit, getters }, new_contact) => {
      return new Promise((resolve, reject) => {
        axios
          .post(`/api/v1/phone-books/${getters[GET_PHONE_BOOK_ID]}/contacts`, new_contact)
          .then(({ data }) => {
            commit(ADD_CONTACT, data)
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    [DELETE_CONTACT]: ({commit, getters}, contact_uuid) => {
      return new Promise((resolve, reject) => {
        axios
          .delete(`/api/v1/phone-books/${getters[GET_PHONE_BOOK_ID]}/contacts/${contact_uuid}`)
          .then(({ data }) => {
            commit(REMOVE_CONTACT, contact_uuid)
            resolve()
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
