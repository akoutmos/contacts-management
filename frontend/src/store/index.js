import Vue from 'vue'
import Vuex from 'vuex'

import PhoneBookStore from './PhoneBook.store'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  strict: debug,
  modules: {
    phone_book: PhoneBookStore
  }
})
